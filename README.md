# Python Exam Grader

## Project Overview

The Python Exam Grader is a web application developed as part of the CS 490 Design in Software Engineering course. This project aims to create a platform for professors to create and automatically grade Python-based exams, providing a streamlined and efficient grading process.

## Features

### Student View:

- **Login:** Students use assigned credentials to access their view.
- **Navigation:** The navigation bar contains three tabs: View Exams, View Grades, and Logout.
- **View Exams:** Displays available exams for students to take.
- **Take Exam:** Students can take open-ended exams and submit their answers.
- **View Grades:** After the professor reviews and posts the grades, students can view their total grades, including detailed feedback and points breakdown.

### Professor View:

- **Login:** Professors use assigned credentials to access their view.
- **Navigation:** The navigation bar contains four tabs: Create Exam, Add Questions to Bank, Review Exams, and Logout.
- **Add Questions to Bank:** Professors can add new questions, search for questions by type, difficulty level, or keywords, and manage the question bank.
- **Create Exam:** Professors can create exams by selecting questions from the question bank, assigning points to each question, and setting the total points.
- **Review Exams:** After students submit their exams, professors can review the auto-graded exams, adjust points, and add comments. Once finalized, professors can release the grades for students to view.

## Tech Stack

- **Backend:** PHP, MySQL
- **Frontend:** HTML, CSS, Vanilla JavaScript
- **AJAX:** For asynchronous data fetching

## Current Status

Please note that this project was developed in 2019 and was hosted on university servers, which are no longer available. As a result, the live application is currently inaccessible and may display a 404 error. Additionally, the code may contain bugs and requires improvements to be fully functional.

## Demo Video

A demo video of the project is available, showcasing its features and functionality. If you would like to see the demo, please feel free to reach out, and I will be happy to share the video with you.

### Usage

- **Professor:**

  - Log in with your assigned credentials.
  - Use the navigation tabs to add questions, create exams, and review submitted exams.

- **Student:**
  - Log in with your assigned credentials.
  - Navigate to view available exams, take exams, and view your grades.

## Contributing

Contributions are welcome! Please fork the repository and create a pull request with your changes. Ensure your code follows the project's coding standards and includes appropriate tests.

## Let's Connect!

If you have any questions, feedback, or ideas for collaboration, don't hesitate to reach out. You can find my contact information in my GitLab profile. Let's continue exploring the fascinating world of software engineering together!
